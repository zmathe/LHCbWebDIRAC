###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json

from DIRAC import gLogger
from DIRAC.Core.Utilities import Time
from DIRAC.Core.DISET.RPCClient import RPCClient
from WebAppDIRAC.Lib.WebHandler import WebHandler, asyncGen


class BookkeepingSimDescriptionHandler(WebHandler):

  AUTH_PROPS = "authenticated"

  def index(self):
    pass

  @asyncGen
  def web_getSelectionData(self):
    data = {"Visible": ['Y', 'N']}
    self.finish(data)

  @asyncGen
  def web_getData(self):

    filter = {}
    limit = 0
    dir = 'DESC'
    sort = 'SimId'
    data = None

    try:
      for f in ["SimId", "SimDescription", "Visible"]:
        jsvalue = json.loads(self.request.arguments.get(f, ['[]'])[-1])

        v = str(jsvalue[0]) if len(jsvalue) > 0 else ''
        if v != '':
          filter[f] = v

      start = int(self.request.arguments.get('start', 0)[-1])
      limit = int(self.request.arguments.get('limit', 0)[-1])
      if 'sort' in self.request.arguments:
        data = json.loads(self.request.arguments['sort'][-1])
        sort = str(data[-1]['property'])
        dir = str(data[-1]['direction'])

    except Exception as e:
      data = {"success": "false", "error": str(e)}

    if limit > 0:
      filter['StartItem'] = start
      filter['MaxItem'] = start + limit

    if dir == 'ASC':
      filter['Sort'] = {'Items': sort, 'Order': 'Asc'}
    else:
      filter['Sort'] = {'Items': sort, 'Order': 'Desc'}

    gLogger.debug("getSimulatuionConditions", str(filter))

    bk = RPCClient("Bookkeeping/BookkeepingManager")
    retVal = yield self.threadTask(bk.getSimulationConditions, filter)

    if not retVal['OK']:
      data = {"success": "false", "error": retVal['Message']}
    else:
      timestamp = Time.dateTime().strftime("%Y-%m-%d %H:%M [UTC]")

      fields = retVal['Value']['ParameterNames']
      totalRecords = retVal['Value']['TotalRecords']
      rows = [dict(zip(fields, i)) for i in retVal['Value']['Records']]
      data = {'success': "true", 'result': rows, 'total': totalRecords, "date": timestamp}
    self.finish(data)

  @asyncGen
  def web_editSimulation(self):

    id = str(self.request.arguments.get('SimId', [''])[-1])
    try:
      id = int(id)
    except Exception:
      self.finish('SimId is not a number')

    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    result = yield self.threadTask(RPC.getSimulationConditions, {'SimId': id})
    if not result['OK']:
      self.finish({"success": "false", "error": result['Message']})
    fields = result['Value']['ParameterNames']
    row = [dict(zip(fields, x)) for x in result['Value']['Records']][0]
    self.finish({"success": "true", "data": row})

  @asyncGen
  def web_simulationinsert(self):
    simdict = dict(self.request.arguments)
    if 'SimId' in simdict:
      del simdict['SimId']
    for i in simdict:
      if isinstance(simdict[i][-1], unicode):
        simdict[i] = str(simdict[i][-1])
      else:
        simdict[i] = simdict[i][-1]
    gLogger.debug("Insert:", str(simdict))

    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    retVal = yield self.threadTask(RPC.insertSimConditions, simdict)
    result = None
    if retVal['OK']:
      result = {"success": "true", "result": "It is registered to the database!"}
    else:
      result = {"success": "false", "error": retVal['Message']}

    self.finish(result)

  @asyncGen
  def web_simulationupdate(self):
    simdict = dict(self.request.arguments)
    for i in simdict:
      if isinstance(simdict[i][-1], unicode):
        simdict[i] = str(simdict[i][-1])
      else:
        simdict[i] = simdict[i][-1]
    gLogger.debug("Insert:", str(simdict))
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    retVal = yield self.threadTask(RPC.updateSimulationConditions, simdict)
    result = None
    if retVal['OK']:
      result = {"success": "true", "result": "It has successfully updated!"}
    else:
      result = {"success": "false", "error": retVal['Message']}

    self.finish(result)

  @asyncGen
  def web_simulationdelete(self):
    id = str(self.request.arguments.get('SimId', [''])[-1])
    try:
      id = long(id)
    except Exception as e:
      self.finish({"success": "false", "error": str(e)})
    gLogger.debug("SimId:", id)
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    retVal = yield self.threadTask(RPC.deleteSimulationConditions, id)
    result = None
    if retVal['OK']:
      result = {"success": "true", "result": "It has successfully deleted!"}
    else:
      result = {"success": "false", "error": retVal['Message']}

    self.finish(result)
