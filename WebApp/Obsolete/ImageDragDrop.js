/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**
 * This plugin provides drag and drop functionality for a {@link Ext.view.View View}. The implementation is similar to {@link Ext.grid.plugin.DragDrop GridDD}.
 * Example:
 * me.view = Ext.create('Ext.view.View', {
 *     store: store,
 *     tpl:tpl,
 *     plugins: {
 *       ptype: 'imagedragdrop',
 *       dragText: 'Drag and drop to reorganize',
 *     }
 *   });
 */
Ext.define('LHCbDIRAC.Accounting.classes.ImageDragDrop', {
  extend: 'Ext.AbstractPlugin',
  alias: 'plugin.imagedragdrop',

  uses: [ 'Ext.view.DragZone', 'Ext.view.DropZone'],

  /**
   * @cfg
   * The text to show while dragging.
   *
   * Two placeholders can be used in the text:
   *
   * - `{0}` The number of selected items.
   * - `{1}` 's' when more than 1 items (only useful for English).
   */
  dragText : '{0} selected image{1}',
  //</locale>

  /**
   * @cfg {String} ddGroup
   * A named drag drop group to which this object belongs. If a group is specified, then both the DragZones and
   * DropZone used by this plugin will only interact with other drag drop objects in the same group.
   */
  ddGroup : "viewDD",

  /**
   * @cfg {String} dragGroup
   * The {@link #ddGroup} to which the DragZone will belong.
   *
   * This defines which other DropZones the DragZone will interact with. Drag/DropZones only interact with other
   * Drag/DropZones which are members of the same {@link #ddGroup}.
   */

  /**
   * @cfg {String} dropGroup
   * The {@link #ddGroup} to which the DropZone will belong.
   *
   * This defines which other DragZones the DropZone will interact with. Drag/DropZones only interact with other
   * Drag/DropZones which are members of the same {@link #ddGroup}.
   */

  /**
   * @cfg {Boolean} enableDrop
   * `false` to disallow the View from accepting drop gestures.
   */
  enableDrop: true,

  /**
   * @cfg {Boolean} enableDrag
   * `false` to disallow dragging items from the View.
   */
  enableDrag: true,

  /**
   * `true` to register this container with the Scrollmanager for auto scrolling during drag operations.
   * A {@link Ext.dd.ScrollManager} configuration may also be passed.
   * @cfg {Object/Boolean} containerScroll
   */
  containerScroll: false,

  init : function(view) {
    view.on('render', this.onViewRender, this, {single: true});
  },

  /**
   * @private
   * AbstractComponent calls destroy on all its plugins at destroy time.
   */
  destroy: function() {
    Ext.destroy(this.dragZone, this.dropZone);
  },

  enable: function() {
    var me = this;
    if (me.dragZone) {
      me.dragZone.unlock();
    }
    if (me.dropZone) {
      me.dropZone.unlock();
    }
    me.callParent();
  },

  disable: function() {
    var me = this;
    if (me.dragZone) {
      me.dragZone.lock();
    }
    if (me.dropZone) {
      me.dropZone.lock();
    }
    me.callParent();
  },

  onViewRender : function(view) {
    var me = this,
    scrollEl;

    if (me.enableDrag) {
      if (me.containerScroll) {
        scrollEl = view.getEl();
      }

      me.dragZone = new Ext.view.DragZone({
        view: view,
        ddGroup: me.dragGroup || me.ddGroup,
        dragText: me.dragText,
        containerScroll: me.containerScroll,
        scrollEl: scrollEl
      });
    }

    if (me.enableDrop) {
      me.dropZone = new Ext.view.DropZone({
        view: view,
        ddGroup: me.dropGroup || me.ddGroup
      });
      me.dropZone.handleNodeDrop = function(data, record, position){
        var view = this.view,
        store = view.getStore(),
        index, records, i, len;
        if (data.copy) {
          records = data.records;
          data.records = [];
          for (i = 0, len = records.length; i < len; i++) {
            data.records.push(records[i].copy());
          }
        } else {
          /*
           * Remove from the source store. We do this regardless of whether the store
           * is the same bacsue the store currently doesn't handle moving records
           * within the store. In the future it should be possible to do this.
           * Here was pass the isMove parameter if we're moving to the same view.
           */
           data.view.store.remove(data.records, data.view === view);
        }
        if (record && position) {
          index = store.indexOf(record);

          // 'after', or undefined (meaning a drop at index -1 on an empty View)...
          if (position !== 'before') {
            index++;
          }
          store.insert(index, data.records);
        }
        // No position specified - append.
        else {
          store.add(data.records);
        }

        view.getSelectionModel().select(data.records); // it is better to not select the image.
      };
    }
  }
});