/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
Ext.define('LHCbDIRAC.Accounting.classes.ImageDragDrop', {
  extend: 'Ext.AbstractPlugin',
  alias: 'plugin.imagedragdrop',

  uses: ['Ext.dd.DragZone', 'Ext.dd.DropZone'],

  //<locale>

  /**
   * @cfg
   * The text to show while dragging.
   *
   * Two placeholders can be used in the text:
   *
   * - `{0}` The number of selected items.
   * - `{1}` 's' when more than 1 items (only useful for English).
   */
  dragText : '{0} selected row{1}',
  //</locale>

  /**
   * @cfg {String} ddGroup
   * A named drag drop group to which this object belongs. If a group is specified, then both the DragZones and
   * DropZone used by this plugin will only interact with other drag drop objects in the same group.
   */
  ddGroup : "GridDD",

  /**
   * @cfg {String} dragGroup
   * The {@link #ddGroup} to which the DragZone will belong.
   *
   * This defines which other DropZones the DragZone will interact with. Drag/DropZones only interact with other
   * Drag/DropZones which are members of the same {@link #ddGroup}.
   */

  /**
   * @cfg {String} dropGroup
   * The {@link #ddGroup} to which the DropZone will belong.
   *
   * This defines which other DragZones the DropZone will interact with. Drag/DropZones only interact with other
   * Drag/DropZones which are members of the same {@link #ddGroup}.
   */

  /**
   * @cfg {Boolean} enableDrop
   * `false` to disallow the View from accepting drop gestures.
   */
  enableDrop: true,

  /**
   * @cfg {Boolean} enableDrag
   * `false` to disallow dragging items from the View.
   */
  enableDrag: true,

  /**
   * `true` to register this container with the Scrollmanager for auto scrolling during drag operations.
   * A {@link Ext.dd.ScrollManager} configuration may also be passed.
   * @cfg {Object/Boolean} containerScroll
   */
  containerScroll: false,

  init : function(view) {
    console.log('Na vegre!!!!');
    view.on('render', this.onViewRender, this, {single: true});
  },

  /**
   * @private
   * AbstractComponent calls destroy on all its plugins at destroy time.
   */
  destroy: function() {
    Ext.destroy(this.dragZone, this.dropZone);
  },

  enable: function() {
    var me = this;
    if (me.dragZone) {
      me.dragZone.unlock();
    }
    if (me.dropZone) {
      me.dropZone.unlock();
    }
    me.callParent();
  },

  disable: function() {
    var me = this;
    if (me.dragZone) {
      me.dragZone.lock();
    }
    if (me.dropZone) {
      me.dropZone.lock();
    }
    me.callParent();
  },

  onViewRender : function(view) {
    console.log('Mia franc???');
    var me = this,
    scrollEl;

    if (me.enableDrag) {
      if (me.containerScroll) {
        scrollEl = view.getEl();
      }

      me.dragZone = new Ext.dd.DragZone(view.getEl(), {

//      On receipt of a mousedown event, see if it is within a DataView node.
//      Return a drag data object if so.
        getDragData: function(e) {

//        Use the DataView's own itemSelector (a mandatory property) to
//        test if the mousedown is within one of the DataView's nodes.
          var sourceEl = e.getTarget(view.itemSelector, 10);

//        If the mousedown is within a DataView node, clone the node to produce
//        a ddel element for use by the drag proxy. Also add application data
//        to the returned data object.
          if (sourceEl) {
            d = sourceEl.cloneNode(true);
            d.id = Ext.id();
            return {
              ddel: d,
              sourceEl: sourceEl,
              repairXY: Ext.fly(sourceEl).getXY(),
              sourceStore: view.store,
              draggedRecord: view.getRecord(sourceEl)
            };
          }
        },

//      Provide coordinates for the proxy to slide back to on failed drag.
//      This is the original XY coordinates of the draggable element captured
//      in the getDragData method.
        getRepairXY: function() {
          return this.dragData.repairXY;
        }
      });
    }

      if (me.enableDrop) {
        me.dropZone = new Ext.dd.DropZone(view.getEl(),{
//        If the mouse is over a target node, return that node. This is
//        provided as the "target" parameter in all "onNodeXXXX" node event handling functions
          getTargetFromEvent: function(e) {
            var node = e.getTarget(view.getItemSelector()),
            mouseY, nodeList, testNode, i, len, box;

//      Not over a row node: The content may be narrower than the View's encapsulating element, so return the closest.
//      If we fall through because the mouse is below the nodes (or there are no nodes), we'll get an onContainerOver call.
        if (!node) {
            mouseY = e.getPageY();
            for (i = 0, nodeList = view.getNodes(), len = nodeList.length; i < len; i++) {
                testNode = nodeList[i];
                box = Ext.fly(testNode).getBox();
                if (mouseY <= box.bottom) {
                    return testNode;
                }
            }
        }
        return node;
          },

//        While over a target node, return the default drop allowed class which
//        places a "tick" icon into the drag proxy.
          onNodeOver : function(node, dragZone, e, data){
            var me = this;

            if (!Ext.Array.contains(data.records, view.getRecord(node))) {
                me.positionIndicator(node, data, e);
            }
            return me.valid ? me.dropAllowed : me.dropNotAllowed;
          },

//        On node drop, we can interrogate the target node to find the underlying
//        application object that is the real target of the dragged data.
//        In this case, it is a Record in the GridPanel's Store.
//        We can use the data set up by the DragZone's getDragData method to read
//        any data we decided to attach.
          onNodeDrop : function(targetNode, dragZone, e, data){  var me = this,
            dropHandled = false,

            // Create a closure to perform the operation which the event handler may use.
            // Users may now set the wait parameter in the beforedrop handler, and perform any kind
            // of asynchronous processing such as an Ext.Msg.confirm, or an Ajax request,
            // and complete the drop gesture at some point in the future by calling either the
            // processDrop or cancelDrop methods.
            dropHandlers = {
                wait: false,
                processDrop: function () {
                    me.invalidateDrop();
                    me.handleNodeDrop(data, me.overRecord, me.currentPosition);
                    dropHandled = true;
                    me.fireViewEvent('drop', targetNode, data, me.overRecord, me.currentPosition);
                },

                cancelDrop: function() {
                    me.invalidateDrop();
                    dropHandled = true;
                }
            },
            performOperation = false;

        if (me.valid) {
            performOperation = me.fireViewEvent('beforedrop', targetNode, data, me.overRecord, me.currentPosition, dropHandlers);
            if (dropHandlers.wait) {
                return;
            }

            if (performOperation !== false) {
                // If either of the drop handlers were called in the event handler, do not do it again.
                if (!dropHandled) {
                    dropHandlers.processDrop();
                }
            }
        }
        return performOperation;
    }
        });
      }
    }
  });