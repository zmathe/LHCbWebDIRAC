/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
Ext.define('LHCbDIRAC.Accounting.classes.ImageModel', {
  extend: 'Ext.data.Model',
  fields: [
           { name:'src', type:'string' },
           { name:'name', type:'string' },
           { name: 'size', type: 'float'},
           { name: 'lastmod', type:'date', dateFormat:'timestamp'},
           { name: 'plotParams', type:'object'}
           ]
});